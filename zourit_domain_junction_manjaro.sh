#!/bin/bash#!/bin/bash

echo "====================================================="
echo "Jonction de poste local au domain zourit : $1"
echo "====================================================="

CONFIG_SSSD=$(cat <<-END
[sssd]
services = nss, pam
config_file_version = 2
domains = default

[nss]
override_shell = /bin/bash

[pam]
offline_credentials_expiration = 60

[domain/default]
enumerate = true
ldap_id_use_start_tls = False
cache_credentials = True
ldap_search_base = o=$1,dc=zourit,dc=re
id_provider = ldap
auth_provider = ldap
chpass_provider = ldap
ldap_uri = ldap://192.168.1.73
ldap_tls_reqcert = never
ldap_search_timeout = 50
ldap_network_timeout = 60
access_provider = simple
END
)


sed -Ei 's/(enable-cache\s*passwd\s*)yes/\1no/' /etc/nscd.conf

sed -Ei 's/(enable-cache\s*group\s*)yes/\1no/' /etc/nscd.conf

sed -Ei 's/(enable-cache\s*netgroup\s*)yes/\1no/' /etc/nscd.conf


yes | pacman -Syu sssd nss-pam-ldapd

echo "$CONFIG_SSSD" > /etc/sssd/sssd.conf
chmod 600 /etc/sssd/sssd.conf


if grep -n
echo 'session         required        pam_mkhomedir.so' /etc/pam.d/common-session ; then
	echo "Trouvé"
else
	echo "Pas trouvé"
	echo "session         required        pam_mkhomedir.so skel=/etc/skel umask=0022" >> /etc/pam.d/common-session
fi

systemctl restart nscd
systemctl restart sssd



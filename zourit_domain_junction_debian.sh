#!/bin/bash

echo "====================================================="
echo "Jonction de poste local au domain zourit : $1"
echo "====================================================="

CONFIG_SSSD=$(cat <<-END
[sssd]
services = nss, pam
config_file_version = 2
domains = default

[nss]
override_shell = /bin/bash

[pam]
offline_credentials_expiration = 60

[domain/default]
enumerate = true
ldap_id_use_start_tls = False
cache_credentials = True
ldap_search_base = o=$1,dc=zourit,dc=re
id_provider = ldap
auth_provider = ldap
chpass_provider = ldap
ldap_uri = ldap://192.168.1.73
ldap_tls_reqcert = never
ldap_search_timeout = 50
ldap_network_timeout = 60
access_provider = simple
END
)

CONFIG_NSCD=$(cat <<-END
debug-level             0
paranoia                no

enable-cache            passwd          no
positive-time-to-live   passwd          600
negative-time-to-live   passwd          20
suggested-size          passwd          211
check-files             passwd          yes
persistent              passwd          yes
shared                  passwd          yes
max-db-size             passwd          33554432
auto-propagate          passwd          yes

enable-cache            group           no
positive-time-to-live   group           3600
negative-time-to-live   group           60        paranoia                no

suggested-size          group           211
check-files             group           yes
persistent              group           yes
shared                  group           yes
max-db-size             group           33554432
auto-propagate          group           yes

enable-cache            hosts           yes
positive-time-to-live   hosts           3600
negative-time-to-live   hosts           20
suggested-size          hosts           211
check-files             hosts           yes
persistent              hosts           yes
shared                  hosts           yes
max-db-size             hosts           33554432

enable-cache            services        yes
positive-time-to-live   services        28800
negative-time-to-live   services        20
suggested-size          services        211
check-files             services        yes
persistent              services        yes
shared                  services        yes
max-db-size             services        33554432

enable-cache            netgroup        no
positive-time-to-live   netgroup        28800
negative-time-to-live   netgroup        20
suggested-size          netgroup        211
check-files             netgroup        yes
persistent              netgroup        yes
shared                  netgroup        yes
max-db-size             netgroup        33554432
END
)

apt install sssd libpam-sss libnss-sss nscd -y

echo "$CONFIG_SSSD" > /etc/sssd/sssd.conf
chmod 600 /etc/sssd/sssd.conf

echo "$CONFIG_NSCD" > /etc/nscd.conf
chmod 600 /etc/nscd.conf

if grep -n 'session    required   pam_mkhomedir.so' /etc/pam.d/common-session ; then
    echo "Trouvé"
else
    echo "Pas trouvé"
    echo "session    required   pam_mkhomedir.so skel=/etc/skel umask=0022" >> /etc/pam.d/common-session
fi

systemctl restart nscd
systemctl restart sssd
